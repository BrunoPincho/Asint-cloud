import requests
import sys
from google.appengine.api import memcache
import json
#import requests_toolbelt.adapters.appengine
#requests_toolbelt.adapters.appengine.monkeypatch()
getter = "https://fenix.tecnico.ulisboa.pt/api/fenix/v1/spaces/"

def recursivo(object,cache,search):
	if isinstance(object, list):
		for element in object:
			#print element['name']
			Cachestr = memcache.get("Rooms")
			Cache = json.loads(Cachestr)
			Cache[element['name'].encode('utf-8')]=element['id']
			memcache.add("Rooms",json.dumps(Cache))
			if search == element['name']:
				#print "encontrou GG"
				#Cache = memcache.get("Roomcache")
				#Cache[element['name'].encode('utf-8')]=element['id']
				#memcache.add("Roomcache",json.dumps(Cache))
				sys.exit()
				return
			r = requests.get(getter + str(element['id']),timeout=10000000)
			recursivo(r.json(),cache,search)
	if isinstance(object, dict):
		for subspace in object['containedSpaces']:
			#print subspace['name']
			Cachestr = memcache.get("Rooms")
			Cache = json.loads(Cachestr)
			Cache[subspace['name'].encode('utf-8')]=subspace['id']
			memcache.add("Rooms",json.dumps(Cache))
			if search == subspace['name']:
				print "encontrou GG"
				#Cache = memcache.get("Roomcache")
				#Cache[subspace['name'].encode('utf-8')]=subspace['id']
				#memcache.add("Roomcache",json.dumps(Cache))
				sys.exit()
				return
			r = requests.get(getter + str(subspace['id']),timeout=10000000)
			recursivo(r.json(),cache,search)

	return

def get_cached(search):
	cache = {}
	r = requests.get(getter)
	recursivo(r.json(),cache,search)
	#return cache


#def main():
#	r = requests.get(getter)
#	recursivo(r.json())
#	print "vai imprimir a cache"
#	f = open('registo_salas.txt','wb')
#	pickle.dump(cache,f)
#	print cache

# teste no pycharm
#main()
