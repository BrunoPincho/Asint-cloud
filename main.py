# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Sample Google App Engine application that demonstrates using the Users API
For more information about App Engine, see README.md under /appengine.
"""
# [START all]
import webapp2
from webapp2_extras import routes
import datetime
import requests
import requests_toolbelt.adapters.appengine
import fenixedu
import json
from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.ext.ndb import polymodel
import urllib2
import urllib
import ast
from google.appengine.api import urlfetch
from recursiv import get_cached
import uuid
import difflib
from threading import Timer
urlfetch.set_default_fetch_deadline(100000)

requests_toolbelt.adapters.appengine.monkeypatch()
config = fenixedu.FenixEduConfiguration.fromConfigFile('fenix.ini')
client = fenixedu.FenixEduClient(config)
data=json.load(open('registo_salas.json'))
memcache.add("Rooms",json.dumps(data))


def task_sweep():
	Tosearch = memcache.get('tasks')
	if Tosearch != "empty":
		get_cached(Tosearch[0])
		emptyset = {}
		memcache.add('tasks',"empty")
	else:
		Timer(240.0,task_sweep).start()

def init_tasks():
	memcache.add('tasks',"empty")
	Timer(180.0,task_sweep).start()

Timer(20.0,init_tasks).start()

class User(polymodel.PolyModel):
	username = ndb.StringProperty(required=True)
	online = ndb.BooleanProperty(required=True)

	@classmethod
	def change_status(cls,name):
		user = cls.query(cls.username==name)
		user = user.get()
		status = user.online
		user.online = not(status)
		user.put()


class Admin(User):
	session_id = ndb.StringProperty(required = True)

	@classmethod
	def check_id(cls,nr):
		admin = cls.query(cls.session_id == nr).get()
		return admin

	@classmethod
	def logout(cls,idn):
		admin = cls.query(cls.session_id==idn).get()
		if admin:
			admin.key.delete()


class Regular(User):
	check = ndb.BooleanProperty(required = True)
	room = ndb.StringProperty(required = False)

	@classmethod
	def check_in(cls,name,room):
		user = cls.query(cls.username==name).get()
		user.check = True
		user.room = room
		user.put()
	@classmethod
	def check_out(cls,name):
		user = cls.query(cls.username==name).get()
		user.check=False
		user.room = None
		user.put()
	@classmethod
	def find_user(cls,name):
		user = cls.query(cls.username==name)
		if(user.count()>0):
			return user.get()
		else:
			return None
	@classmethod
	def logout(cls,name):
		user = cls.query(cls.username==name).get()
		user.online = False
		if user.room:
			Room.unregister_user(user.room,user.username)


class Room(ndb.Model):
	occupation = ndb.IntegerProperty(required = True)
	Rname = ndb.StringProperty(required = True)
	userslisted = ndb.JsonProperty(required=False)

	@classmethod
	def get_occupied(cls):#vai retornar um query object que vai ter de ser iterado
		rooms = cls.query(cls.occupation>=1)
		return rooms

	@classmethod
	def register_user(cls,roomn,name):
		room = cls.query(cls.Rname == roomn).get()
		if room:
			temp = room.userslisted
			loaded = json.loads(temp)
			loaded[name]=True
			room.userslisted = json.dumps(loaded)
			room.occupation += 1
			room.put()
			log = checklog(room=roomn,user = name,move = "check in",timestamp=str(datetime.datetime.today()))
			log.put()
			Regular.check_in(name,roomn)
			return room
		else:
			return None

	@classmethod
	def unregister_user(cls,roomn,name):
		room = cls.query(cls.Rname == roomn).get()
		temp = room.userslisted
		loaded = json.loads(temp)
		uniname = name.encode('utf-8') #na memcache a codificacao passa a unicode, por isso as strings tem de ser codificadas novamente
		del loaded[uniname]
		room.userslisted = json.dumps(loaded)
		room.occupation -= 1
		room.put()
		Regular.check_out(name)
		log = checklog(room=roomn,user = name,move = "check out",timestamp=str(datetime.datetime.today()))
		log.put()

	@classmethod
	def get_occupancy(cls,room):
		room = cls.query(cls.Rname == room).get()
		return room.occupation
	@classmethod
	def get_room(cls,room):
		room = cls.query(cls.Rname == room).get()
		if room:
			return room
		else:
			None

class msg(ndb.Model):
	msg = ndb.StringProperty(required=True)
	destUser = ndb.StringProperty(required=True)

	@classmethod
	def get_msg(cls,UserName):
		buff = cls.query(cls.destUser==UserName)
		if(buff.count()>0):
			return buff
		else:
			return None


class checklog(ndb.Model):
	room = ndb.StringProperty(required=True)
	user = ndb.StringProperty(required=True)
	move = ndb.StringProperty(required=True)
	timestamp = ndb.StringProperty(required=True)

	@classmethod
	def get_logs(cls,roomname):
		logs = cls.query(cls.room == roomname)
		if(logs.count()>0):
			return logs
		else:
			return None


def sendFile(response, FileName):
	response.headers.add_header('Content-Type', mimetypes.guess_type(FileName)[0])
	f = open(FileName, 'r')
	response.write(f.read())
	f.close()

class landing(webapp2.RequestHandler):
	def get(self):
		code = self.request.GET['code']
		#config = fenixedu.FenixEduConfiguration.fromConfigFile('fenix.ini')
		#client = fenixedu.FenixEduClient(config)
		user = client.get_user_by_code(code)
		person = client.get_person(user)
		#client_id="288540197912628"
		#client_secret="+H1Is6IrWDO+/9HlZEFGergRNP46xrYfJNVb1dlnfcu0S3QjXhimyBzuR+4IiahEkWPLxOAknjX13fP7lfpBGA=="
		#redirect_uri = "https://asint-187818.appspot.com/landingHub"
		#url = "https://fenix.tecnico.ulisboa.pt/oauth/access_token"#?client_id=288540197912628&client_secret="+client_secret+"&redirect_uri=https://asint-187818.appspot.com/landingHub&code="+str(code)+"&grant_type=authorization_code"
		#r_params = {'client_id':client_id,
		#	'client_secret':client_secret,
		#	'redirect_uri':redirect_uri,
		#	'code':code,
		#	'grant_type':'authorization_code'}
		#r_header = {'content-type':'application/x-www-form-urlencoded'}
		#r = urlfetch.fetch(url,headers = r_header,method='POST',payload = urllib.urlencode(r_params))
		#self.response.write(r.content)
		#response = json.loads(r.content)
		#access_token = response['access_token']
		#token_type = response['token_type']
		#token_expires = response['expires_in']

		#p_header = {'content-type':'application/x-www-form-urlencoded'}
		#p_params= {'access_token':access_token}
		#p_url = "https://fenix.tecnico.ulisboa.pt/api/fenix/v1/person"
		#s = requests.get(p_url,params=p_params)
		if Regular.find_user(person['name']):
			Regular.change_status(person['name'])
		else:
			user = Regular(check=False,room=None,username=person['name'],online=True)
			user.put()
		#manda o form
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		f = open('form.html', 'r')
		tempdata = f.read()
		s = tempdata.replace('$name_user', person['name'])
		self.response.write(s)


	def options(self, *args, **kwargs):
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

	def put(self):
		js = self.request.body
		x = ast.literal_eval(js)
		CommUser = x["from"]
		user = Regular.find_user(CommUser)
		if user:
			if user.online == True:
				msglist = {}
				i = 0
				mails = msg.get_msg(CommUser)
				if mails:
					for mail in mails:
						msglist[i]=mail.msg
						mail.key.delete()
						i+=1
					#msg.clean_msg(CommUser)
					#ndb.delete_multi(mails)
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps({"Data":msglist}))
				else:
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps({"Data":"no messages"}))
			else:
				self.response.write("login error")
				self.response.set_status(403)
		else:
			self.response.write("login error")
			self.response.set_status(405)


#test zone
class fill_cache(webapp2.RequestHandler):
	def get(self):
		data = {}
		#data = get_cached()
		memcache.add("Roomcache",data)
		temp = memcache.get("Roomcache")
		self.response.write(json.dumps(temp))
		#data=json.load(open('registo_salas.json'))
		#memcache.add("Roomcache",data)
		#temp = memcache.get("Roomcache")
		#self.response.write(json.dumps(temp))

class Verify(webapp2.RequestHandler):
    	def get(self):
		accstr='https://fenix.tecnico.ulisboa.pt/oauth/userdialog?client_id=288540197912628&redirect_uri=https://asint-187818.appspot.com/landingHub'
		self.redirect(accstr)

class testjson(webapp2.RequestHandler):
	def get(self,token):
		self.response.write(token)


	def put(self):
		js = self.request.body
		x = ast.literal_eval(js)
		self.response.headers['Content-Type']='application/json'
		bulk = self.request.body
		parsed = json.loads(bulk)
		x = Admin.check_id(parsed["token"])
		self.response.write(x)

class Roomhandler(webapp2.RequestHandler):
	def put(self):
		js = self.request.body
		x = ast.literal_eval(js)
		CommUser = x["from"]
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		sala = str(x["space"])
		User = Regular.find_user(CommUser)
		if User:
			if User.online==True:
				RoomCache = memcache.get("Rooms")
				cache = json.loads(RoomCache)
				dataset = cache.keys()
				matchset = difflib.get_close_matches(sala,dataset)
				if sala in cache.keys():
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps([client.get_space(cache[sala])]))
				elif len(matchset)>0:
					out = []
					for key in matchset:
						out.append(client.get_space(cache[key]))
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps(out))
				else:
					#thread = threading.Thread(target=search_task(sala))
					#thread.start()
					memcache.add("tasks",sala.encode('utf-8'))
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps([{"name":"not Found"}]))
					#get_cached(sala)
					#procurar pela sala
			else:
				self.response.headers['Content-Type']='application/json'
				self.response.write(json.dumps([{"name":"login error"}]))
		else:
			self.response.headers['Content-Type']='application/json'
			self.response.write(json.dumps([{"name":"login error"}]))

	def options(self, *args, **kwargs):
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		self.response.headers['Access-Control-Allow-Methods'] = 'GET,PUT,DELETE'

	def post(self):
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		jsname = self.request.body
		x = ast.literal_eval(jsname)
		CommUser = x["from"]
		roomname=x["space"]
		User = Regular.find_user(CommUser)
		if User:
			if User.online == True :
				if User.check==False:
					room = Room.register_user(roomname,CommUser)
					if room:
						self.response.headers['Content-Type']='application/json'
						self.response.write(json.dumps([{"name":room.Rname ,"capacity":room.occupation}]))
					else:
						listuser = json.dumps({CommUser:True})
						room = Room(occupation=1,userslisted = listuser,Rname = roomname)
						room.put()
						log = checklog(room=roomname,user = CommUser,move = "check in",timestamp=str(datetime.datetime.today()))
						log.put()
						Regular.check_in(CommUser,roomname)
						self.response.headers['Content-Type']='application/json'
						self.response.write(json.dumps([{"name":roomname ,"capacity":room.occupation}]))
				else:
					Room.unregister_user(User.room,CommUser)
					room = Room.register_user(roomname,CommUser)
					if room:
						self.response.headers['Content-Type']='application/json'
						self.response.write(json.dumps([{"name":room.Rname ,"capacity":room.occupation}]))
					else:
						listuser = json.dumps({CommUser:True})
						room = Room(occupation=1,userslisted = listuser,Rname = roomname)
						room.put()
						log = checklog(room=roomname,user = CommUser,move = "check in",timestamp=str(datetime.datetime.today()))
						log.put()
						Regular.check_in(CommUser,roomname)
						self.response.headers['Content-Type']='application/json'
						self.response.write(json.dumps([{"name":room.Rname ,"capacity":room.occupation}]))


	def delete(self):
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		jsname = self.request.body
		x = ast.literal_eval(jsname)
		CommUser = x['from']
		roomname=x['space']
		User = Regular.find_user(CommUser)
		if User.online == True :
			Room.unregister_user(roomname,CommUser)
			self.response.write(json.dumps({"resp":"OK"}))


class logouter(webapp2.RequestHandler):
	def get(self):
		self.response.write("<h1>Good Bye</h1>")


class Userlogin(webapp2.RequestHandler):
	def get(self):
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		f = open('login.html', 'r')
		tempdata = f.read()
		self.response.write(tempdata)
		#sendFile(self.response,'login.html')

	def options(self, *args, **kwargs):
	      self.response.headers['Access-Control-Allow-Origin'] = '*'
	      self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
	      self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

	def put(self):
		#nome = self.request.json['username']
		#password = self.request.json['password']
		jsname = self.request.body
		x = ast.literal_eval(jsname)
		nome = x['username']
		password = x['password']
		self.response.headers['Content-Type']='application/json'
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		if nome == "admin" and password=="123":
			token = str(uuid.uuid1())
			token = token.encode('utf-8')
			new_admin = Admin(username = "admin",online=True,session_id=token)
			new_admin.put()
			loadobj = '/adminpage/'+token
			self.response.write(json.dumps({'load':loadobj}))
		else:
			self.response.write(json.dumps({'load':'BAD'}))
		#if nome in registered_users.keys():
		#	if registered_users[nome].password==password and registered_users[nome].online==False:
		#			print ("entrou")
		#			registered_users[nome].change_status()
		#			cookieval = self.request.headers.get("Cookie")
		#			sessions[cookieval] = registered_users[nome]
		#			self.response.write(json.dumps({'load':'/searchform'}))
		#	else:
		#			self.response.write(json.dumps({'load':'BAD'}))
		#else:
		#	self.response.write(json.dumps({'load':'BAD'}))

	def delete(self):
		self.response.headers['Content-Type']='application/json'
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		jsname = self.request.body
		x = ast.literal_eval(jsname)
		CommUser = x["from"]
		if(CommUser!="admin"):
			Regular.logout(CommUser)
		else:
			Admin.logout(x["token"])

		self.response.write(json.dumps({"resp":"all good","load":"/logout"}))

class Adminhandler(webapp2.RequestHandler):
	def get(self,token):
		self.response.headers.add_header('Access-Control-Allow-Origin', '*')
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		#f = open('adminLandpage.html', 'r')
		#tempdata = f.read()
		#token = str(uuid.uuid1())
		#token = token.encode('utf-8')
		#new_admin = Admin(username = "admin",online=True,session_id=token)
		#new_admin.put()
		tokenp = token
		adminobj=Admin.check_id(tokenp)
		if adminobj:
			f = open('adminLandpage.html', 'r')
			tempdata = f.read()
			s = tempdata.replace('$admin_id',token)
			self.response.write(s)
		else:
			self.response.write("No authorization, not admin")

	def options(self, *args, **kwargs):
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

	def put(self):
		self.response.headers['Content-Type']='application/json'
		self.response.headers['Access-Control-Allow-Origin'] = '*'
		self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		bulk = self.request.body
		x = ast.literal_eval(bulk)
		Opert = x["operation"]
		token =x["token"]
		adminobj=Admin.check_id(token)
		if adminobj:
			if Opert == "showrooms":
				OutputJson = {}
				roomList = Room.get_occupied()
				for sala in roomList:
					OutputJson[sala.Rname]=json.loads(sala.userslisted)

				self.response.write(json.dumps(OutputJson))
			if Opert == "msg":
				nova = msg(msg=x["string"],destUser=x["dest"])
				nova.put()
				self.response.headers['Content-Type']='application/json'
				self.response.write(json.dumps({"resp":"all good"}))
			if Opert == "logs":
				logs = checklog.get_logs(x["space"])
				output={}
				i = 0
				if logs:
					for log in logs:
						output[i] = "sala: "+log.room +" user: "+log.user+" move: " +log.move +" datetime: "+ log.timestamp+"\n"
						i+=1
					#load = json.dumps(output)
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps({"Data":output}))
				else:
					self.response.headers['Content-Type']='application/json'
					self.response.write(json.dumps({"Data":"no logs"}))
		else:
			self.response.headers['Content-Type']='application/json'
			self.response.write(json.dumps({"Data":"error no admin"}))



app = webapp2.WSGIApplication([
	('/',Verify),
	('/landingHub', landing),
	(r'/roomManager',Roomhandler),
	(r'/login', Userlogin),
	webapp2.Route('/adminpage',Adminhandler),
	webapp2.Route('/adminpage/<token>',Adminhandler,methods=['GET']),
	webapp2.Route(r'/jsonP',testjson),
	webapp2.Route(r'/jsonP/<token>',testjson),
	(r'/logout',logouter),
], debug=True)

# [END all]
